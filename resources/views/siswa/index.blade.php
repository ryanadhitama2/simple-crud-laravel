@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data Siswa</div>

                <div class="card-body">
                    <a href="{{route('siswa.create')}}" class="btn btn-success">Tambah Data</a>
                    <br> <br>
                    @if(Session::has('info'))
                        <div class="alert alert-info">
                            {{Session::get('info')}}
                        </div>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($models as $key => $data)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->age}}</td>
                                    <td>
                                        <a href="{{route('siswa.edit', ['id'=>$data->id])}}" class="btn btn-sm btn-warning">Edit</a>
                                        <form action="{{route('siswa.destroy', ['id'=>$data->id])}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

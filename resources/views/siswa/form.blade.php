@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@if(!$model->exists) Add @else Edit @endif Siswa</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <form action="@if($model->exists) {{route('siswa.update',['id'=>$model->id])}} @else {{route('siswa.store')}} @endif" method="POST">
                                @csrf
                                @method($model->exists ? 'PUT' : 'POST')

                                <div class="form-group">
                                        <label class="form-label" for="">Name</label>
                                        <input type="text" name="name" id="" class="form-control" value="{{ old('name', $model->name) }}">
                                </div>

                                <div class="form-group">
                                        <label class="form-label" for="">Age</label>
                                        <input type="number" name="age" value="{{ old('age', $model->age) }}" class="form-control">
                                </div>

                                <button type="submit" class="btn btn-success">Simpan Data</button>
                        </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use Validator;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $models = Siswa::all();
        return view('siswa.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $model = new Siswa();
        return view('siswa.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator      = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'age'      => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'     => 'Nama',
                'age'      => 'Umur'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $model = new Siswa();
        $model->name = $request->name;
        $model->age = $request->age;
        $model->save();

        return redirect()->route('siswa.index')->with('info', 'Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Siswa::findOrFail($id);
        return view('siswa.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator      = Validator::make(
            $request->all(),
            [
                'name'          => 'required',
                'age'      => 'required',
            ],
            [
                'required'          => ':attribute tidak boleh kosong.',
                'email'             => ':attribute harus dengan format email contoh: agus@gmail.com',
                'numeric'           => ':attribute harus berupa angka',
                'max'               => 'Ukuran gambar maksimal 1024kb '
            ],
            [
                'name'     => 'Nama',
                'age'      => 'Umur'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $model = Siswa::findOrFail($id);
        $model->name = $request->name;
        $model->age = $request->age;
        $model->save();

        return redirect()->route('siswa.index')->with('info', 'Data Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $model = Siswa::findOrFail($id);
        $model->delete();

        return redirect()->back()->with('info', 'Data Berhasil dihapus');
    }
}
